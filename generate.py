import csv
import datetime as dt
import json
import os
import re
import shutil
import sys
import time
import zipfile
from collections import defaultdict

import requests
from OSMPythonTools.nominatim import Nominatim
from OSMPythonTools.overpass import overpassQueryBuilder, Overpass
from geojson import Feature, FeatureCollection, Point

from streetname_map import streetname_map

runtime_start = time.time()

nominatim = Nominatim()
overpass = Overpass()

features = defaultdict(list)

menu_data = defaultdict(lambda: defaultdict(lambda: defaultdict()))

only_missing = False

ADRESY_ZIP_URL = "https://proxy.freemap.sk/minvskaddress/adresy.zip"
ADRESY_ZIP = "adresy.zip"

replacement_map = streetname_map.replacement_map
muni_replacement_map = streetname_map.muni_replacement_map


def calc_process_time(starttime, cur_iter, max_iter):
    # stolen from https://stackoverflow.com/a/58450938 :)
    telapsed = time.time() - starttime
    testimated = (telapsed / cur_iter) * max_iter

    finishtime = starttime + testimated
    finishtime = dt.datetime.fromtimestamp(finishtime).strftime("%H:%M:%S")
    lefttime = testimated - telapsed

    return int(telapsed), int(lefttime), finishtime


def remove_data():
    print("Removing data")
    # shutil.rmtree("address", ignore_errors=True)
    shutil.rmtree("data", ignore_errors=True)
    shutil.rmtree("cache", ignore_errors=True)


def download_address_zip():
    print("Downloading adresy.zip")
    with open(ADRESY_ZIP, "wb") as f:
        f.write(requests.get(ADRESY_ZIP_URL).content)


def unzip_file():
    print("Unziping adresy.zip")
    os.makedirs("data", exist_ok=True)
    with zipfile.ZipFile(ADRESY_ZIP, 'r') as zip_ref:
        zip_ref.extractall("data")
    os.remove("adresy.zip")


def prepare_data():
    print("Preparing data")
    filename = select_file_to_parse()
    last_id = find_last_id(f"data/{filename}")
    with open(f"data/{filename}", newline='', encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        # skip first line
        next(reader)
        cities = defaultdict(int)
        start_time = time.time()
        for idd, refminvsk, kraj, okres, city, suburb, street, consnum, streetnum, postcode, longitude, latitude, changedat, municipalityCode in reader:
            if municipalityCode not in cities.keys():
                cities[municipalityCode] = 1
            else:
                cities[municipalityCode] += 1
            
            if city not in menu_data[kraj][okres]:
                menu_data[kraj][okres][city] = municipalityCode
            
            if int(idd) % 10000 == 0:
                print_status(start_time, int(idd), last_id)
        
    print_status(start_time, int(idd), last_id)
    print("\nSaving data")
    with open("data/data.json", 'w', encoding="UTF-8") as f:
        f.write(json.dumps(cities))
    with open("data/menu_data.json", 'w', encoding="UTF-8") as f:
        f.write(json.dumps(menu_data))


def find_last_id(filename):
    # stolen from https://stackoverflow.com/a/54278929 :)
    with open(filename, 'rb') as f:
        try:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b'\n':
                f.seek(-2, os.SEEK_CUR)
        except OSError:
            f.seek(0)
        return int(f.readline().decode().split(',')[0])


def print_status(start_time, iteration, last_id):
    print(f"{iteration}/{str(last_id)} {round((iteration / last_id) * 100, 1)}% "
          + "time elapsed: %ss, time left: %ss, estimated finish time: %s    "
          % calc_process_time(start_time, iteration, last_id), end="\r")


def sort_elements(e):
    sort = [e.tag("addr:suburb"), e.tag("addr:street"), e.tag("addr:conscriptionnumber"), e.tag("addr:streetnumber")]
    sort = [x if x is not None else "0" for x in sort]
    return sort


def parse_file(addr_filename, city_name=None, city_id=None, all_cities=False, only_missing=False):
    print(f"Parsing {addr_filename}")

    source_tag = f"minvskaddress {addr_filename[7:14]}"

    addr_filename = f"data/{addr_filename}"

    if all_cities:
        last_id = find_last_id(addr_filename)
    else:
        with open("data/data.json", 'r', encoding="UTF-8") as f:
            data = json.load(f)
        if city_name is not None:
            city_id = menu_data[city_name[0]][city_name[1]][city_name[2]]
        last_id = int(data[city_id])

    area_id = None
    current_city = None
    iteration = 0
    
    with open(addr_filename, newline='', encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        # skip first line
        next(reader)

        start_time = time.time()
        for idd, refminvsk, kraj, okres, city, suburb, street, consnum, streetnum, postcode, longitude, latitude, changedat, municipalityCode in reader:
            if city_id == municipalityCode or all_cities:
                iteration += 1
                try:
                    filename = f"{'missing/' if only_missing else ''}{kraj}/{okres}/{city}{f'_{suburb}' if suburb != '' and suburb != city else ''}_{municipalityCode}"
                    lati, longi = map(float, (str(latitude).strip().replace(',', '.'), str(longitude).strip().replace(',', '.')))
                except ValueError:
                    if latitude == "" or longitude == "":
                        lati, longi = map(float, (0, 0))
                        filename = f"error/{kraj}/{okres}/{city}_{municipalityCode}"

                props = defaultdict()

                props['ref:minvskaddress'] = refminvsk
                props['addr:country'] = 'SK'
                props['source:addr'] = source_tag

                if city.startswith("Bratislava") or city.startswith("Košice"):
                    city, suburb = city.split('-', maxsplit=1)
                
                if city != "":
                    props['addr:city'] = city

                if street != "":
                    props['addr:street'] = normalize_streetname(street, city)
                    if suburb != "":
                        props['addr:suburb'] = suburb
                else:
                    props['addr:place'] = suburb if suburb != "" else city

                if consnum != "":
                    props['addr:conscriptionnumber'] = consnum
                    props['addr:housenumber'] = consnum

                if streetnum != "":
                    props['addr:streetnumber'] = streetnum
                    props['addr:housenumber'] = f"{consnum}/{streetnum}"

                if postcode != "":
                    props['addr:postcode'] = postcode

                if only_missing:
                    if area_id is None or current_city != city:
                        current_city = city
                        elements = None
                        while elements is None:
                            try:
                                area_id = nominatim.query((current_city if current_city != "Bratislava" or current_city != "Košice" else f"{current_city} {suburb}") + ', ' + kraj + ', Slovakia').areaId()
                                query = overpassQueryBuilder(area=area_id, elementType=['node', 'way', 'relation'], selector='~"^addr:.*$"~"."', out='center body')
                                result = overpass.query(query, timeout=60)
                                elements = result.elements()
                                elements = sorted(elements, key=sort_elements)
                            except:
                                continue
                    
                    if iteration % 10 == 0:
                        print_status(start_time, iteration, last_id)

                    add = True
                    for element in elements:
                        if element.tags() is not None:
                            pr = props.copy()
                            for pk, pv in props.items():
                                if pk == "source:addr":
                                    if str(element.tag(pk)).startswith("minvskaddress"):
                                        pr.pop(pk)
                                elif element.tag(pk) == pv:
                                    pr.pop(pk)
                            
                            if len(pr) == 0:
                                add = False
                                elements.remove(element)
                                break
                    
                    if not add:
                        continue

                features[filename].append(
                    Feature(
                        geometry=Point((longi, lati)),
                        properties=props
                    )
                )

            if int(idd) % 10000 == 0:
                if iteration != 0:
                    print_status(start_time, iteration, last_id)
                elif last_id > iteration:
                    print("Searching for selected city...", end='\r')
            if last_id == iteration:
                if only_missing and not all_cities:
                    for element in elements:
                        try:
                            longi, lati = map(float, (element.lon(), element.lat()))
                        except:
                            longi, lati = map(float, (element.centerLon(), element.centerLat()))

                        features[f"not_in_minvsk/{current_city}"].append(
                            Feature(
                                geometry=Point((longi, lati)),
                                properties=element.tags()
                            )
                        )
                break

    print_status(start_time, iteration, last_id)
    print(f"\nDONE parsing in {round(time.time() - runtime_start, 1)} s")


def normalize_streetname(streetname, muni):
    """oprav nazov ulice `streetname` podla kluca nizsie"""
    # from https://github.com/FreemapSlovakia/freemap-operations/tree/master/mass_edits/import_adries_minv
    norm_name = streetname
    # vyhod ul./ulica na konci nazvu ulice
    norm_name = re.sub(r"(.*) ([uU]l\.|[uU]lica)(\b|$)", r"\1", norm_name)
    # za medzeru patri vzdy a prave jedna medzera
    norm_name = re.sub(r"\.(?=[^ ])", ". ", norm_name)
    # vyhod medzery na zaciatku a konci
    norm_name = norm_name.strip()
    # rozsir nam. na namestie
    norm_name = re.sub(r"([nN])ám\.", r"\1ámestie", norm_name)
    # vyhod ul./ulica na zaciatku nazvu ulice a prve pismeno zmen na velke
    norm_name = re.sub(
        r"^([uU]l\. *|[uU]lica )(.*)",
        lambda n: n.group(2)[0].upper() + n.group(2)[1:],
        norm_name,
    )
    # 2 a viac medzier vymen za jednu
    norm_name = re.sub(r"  *", " ", norm_name)
    # Nam/Namestie/Mieru/Partizán/Slobody/Rad/Cesta/Park/Gen./Kpt./Detí/Vrch/
    # Ihrisk/Mládeže/Hrdinov/Lúky/Aleja/Koniec/Majer/Rieke/Háj/Vodami/Budovateľ/
    # Brehu/Dolin/Kameni/Stanicu/Osada/Bitúnku/Dobrovoľníkov/Strana/Dedina/
    # Bojovníkov/Trh/Humnami/Strelnic/Trieda/Jama/Zábava/Kúpele/Kameň/Kríž/Potok/
    # Sad/Riadok/Hora/Veža/Kopanice/Armády/Nábr/Skala/Pažiť/Pole/Priekop/Les
    # je velkym zaciatocnym pismenom len na zaciatku nazvu
    norm_name = re.sub(
        r" ("
        "Aleja|"
        "Nár.*Pov|"
        "Amfiteát|"
        "Armády|"
        "Baštou|"
        "Bitúnku|"
        "Záhrad|"
        "Stráňa|"
        "Bojovníkov|"
        "Pionierov|"
        "Brán(a|ou)|"
        "Rybník|"
        "Rybníčkoch|"
        "Brázdach|"
        "Brehu*|"
        "Budovateľ|"
        "Cechy|"
        "Cesta|"
        "Colnici|"
        "Dedina|"
        "Detí|"
        "Diaľnic|"
        "Doba|"
        "Dobrovoľníkov|"
        "Dolin(e|a|y)|"
        "Dolnom Konci|"
        "Dubu|"
        "Dukelských|"
        "Dvor|"
        "Gen\.|"
        "Háj|"
        "Hámor|"
        "Hora|"
        "Hôrkami|"
        "Hostinci|"
        "Hradbami|"
        "Hrádzou|"
        "Hrbe|"
        "Hraničiarov|"
        "Hrdinov|"
        "Hrebienkom|"
        "Humnami|"
        "Chrbát|"
        "Ihrisk|"
        "Jama|"
        "Jark(om|y)|"
        "Jaskyni|"
        "Jazdiarni|"
        "Jazero|"
        "Kameň|"
        "Kameni|"
        "Kanálom|"
        "Kláštorom|"
        "Kockách|"
        "Kolíske|"
        "Koniec|"
        "Kopanice|"
        "Kope$|"
        "Kpt\.|"
        "Kríž|"
        "Kúpele|"
        "Kúriou|"
        "Kút|"
        "Kúte|"
        "Kútoch|"
        "Laz|"
        "Lehote|"
        "Les|"
        "Lodenici|"
        "Lúčka|"
        "Lúky|"
        "Majer(\b|i|$)|"
        "Martýrov|"
        "Mestom|"
        "Mieru|"
        "Mládeže|"
        "Mlyn|"
        "Motorest|"
        "Múrom|"
        "Nábr|"
        "Nádvor|"
        "Nám(\.|estie)|"
        "Nivy|"
        "Ohradami|"
        "Ohrade|"
        "Okolie|"
        "Okruhliak|"
        "Osada|"
        "Ostrovy|"
        "Panoráme|"
        "Park|"
        "Partizán|"
        "Paseka|"
        "Pasienkoch|"
        "Paž[ií][ťt]|"
        "Peci|"
        "Pílu|"
        "Pleso|"
        "Plynárni|"
        "Pole|"
        "Polícii|"
        "Pošte|"
        "Potočky|"
        "Potok|"
        "Povst|"
        "Prameni|"
        "Predmest|"
        "Priekop|"
        "Pustatina|"
        "Rad$|"
        "Rašelin|"
        "Riadk(och|u)|"
        "Riadok|"
        "Riek(e|ou)|"
        "Rodiny|"
        "Roh$|"
        "Role|"
        "Rovn|"
        "Rožku|"
        "Ruže|"
        "Sad|"
        "Salaši|"
        "Skala|"
        "Skalám|"
        "Skalke|"
        "Slobody|"
        # Smrečine  (Smrečina = fabrika)
        "Splave|"
        "Stanicu|"
        "Strana|"
        "Stráňami|"
        "Stráňou|"
        "Strelnic|"
        "Studn|"
        "Studničke|"
        "Tehelňu|"
        "Terasy|"
        "Trh(\b|$)|"
        "Trieda|"
        "Ulička|"
        "Uličkou|"
        "Veža|"
        "Vila|"
        "Vinic|"
        "Vinohradoch|"
        "Vinohrady|"
        "Vodami|"
        "Vrch|"
        "Vŕškami|"
        "Vŕšku|"
        "Vršky|"
        "Zábava|"
        "Záhumenice|"
        "Zámočku"
        ")",
        lambda n: n.group(0).lower(),
        norm_name,
    )
    # Sv/Svateho je velkym zaciatocnym pismenom len na zaciatku nazvu
    norm_name = re.sub(r" Sv(\.|ät)", r" sv\1", norm_name)
    # zaciatocne pismeno je vzdy velke
    norm_name = re.sub(r"^(.)", lambda n: n.group(0).upper(), norm_name)
    # nazvy mesiacov za bodkou su malym (11. marca, 8. maja..)
    norm_name = re.sub(r"\. (Mája|Marca)", lambda n: n.group(0).lower(), norm_name)

    # zname chyby a preklepy oprav podla pomocnej mapy
    if norm_name in replacement_map.keys():
        norm_name = replacement_map[norm_name]
    
    if muni in muni_replacement_map.keys():
        norm_name = normalize_streetname_city(norm_name, muni)

    return norm_name


def normalize_streetname_city(streetname, muni):
    norm_name = streetname

    for bad_name, good_name in muni_replacement_map[muni].items():
        if streetname == bad_name:
            norm_name = good_name
    return norm_name


def fix_street_names(features_to_fix):
    for feature in features_to_fix['features']:
        feature["properties"]["addr:street"] = normalize_streetname(feature["properties"]["addr:street"])

        for muni in muni_replacement_map.keys():
            for bad_name, good_name in muni_replacement_map[muni].items():
                if feature["properties"]["addr:city"] == muni and feature["properties"]["addr:street"] == bad_name:
                    feature["properties"]["addr:street"] = good_name


def save_to_file(features_to_save):
    files_left = len(features_to_save.items())
    print(f"Saving {files_left} files")

    for code, feats in features_to_save.items():
        collection = FeatureCollection(feats)

        try:
            with open(f"address/{code}.geojson", "w") as f:
                f.write('%s' % collection)
        except FileNotFoundError:
            folder = code.rsplit('/', 1)[0]
            os.makedirs("address/" + folder)
            with open(f"address/{code}.geojson", "w") as f:
                f.write('%s' % collection)

        print(f"{files_left} files left       ", end="\r")
        files_left -= 1

    print("DONE all files saved")


def add_info_file(addr_filename):
    print("Adding info file")
    with open('address/info.txt', 'w') as f:
        f.write(f"""generated at {dt.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}
minvsk data from {addr_filename[7:17]}
""")


def menu_select(data):
    for i, v in enumerate(data):
        print(f"{i} - {v}")

    try:
        ret = list(data)[int(input("Select: "))]
    except (ValueError, IndexError):
        ret = menu_select(data)

    return ret


def select_file_to_parse():
    try:
        addr_filenames = os.listdir("data")
    except FileNotFoundError:
        print("Data folder not found.\nRun again with download command.")
        exit()

    addr_filenames = list(filter(re.compile("^Adresy_[0-9]{4}-[0-9]{2}-[0-9]{2}.csv$").match, addr_filenames))

    if len(addr_filenames) == 0:
        print("No matching file found.\nRun again with download command.")

    file_index = None
    if len(addr_filenames) == 1:
        file_index = 0
        filename = addr_filenames[file_index]
    while file_index is None:
        try:
            for i, f in enumerate(addr_filenames):
                print(f"{i} - {f}")
            file_index = int(input("Select file: "))

            filename = addr_filenames[file_index]
        except (ValueError, IndexError):
            file_index = None

    if not re.fullmatch(re.compile("^Adresy_[0-9]{4}-[0-9]{2}-[0-9]{2}.csv$"), filename):
        print("File name does NOT match regex")
        exit()

    return filename


# switch to save only addresses which are not in OSM
if len(sys.argv) > 1:
    if sys.argv[1] == "-m":
        only_missing = True
        sys.argv.remove("-m")

# show menu to select city
if len(sys.argv) == 1:
    addr_filename = select_file_to_parse()

    with open("data/menu_data.json", 'r', encoding="UTF-8") as f:
        menu_data = json.load(f)

    sel_kraj = menu_select(menu_data.keys())
    sel_okres = menu_select(menu_data[sel_kraj].keys())
    sel_mesto = menu_select(menu_data[sel_kraj][sel_okres])

    parse_file(addr_filename, city_name=[sel_kraj, sel_okres, sel_mesto], only_missing=only_missing)
    save_to_file(features)

# generate address files for all cities
elif sys.argv[1] == 'all':
    addr_filename = select_file_to_parse()
    parse_file(addr_filename, all_cities=True, only_missing=only_missing)
    save_to_file(features)
    add_info_file(addr_filename)

# remove all data
elif sys.argv[1] == 'remove':
    remove_data()

elif sys.argv[1] == 'download':
    download_address_zip()
    unzip_file()
    prepare_data()

# generate address file for selected city
elif len(sys.argv) == 2 and re.fullmatch(re.compile("^SK[0-z]{10}$"), sys.argv[1]):
    addr_filename = select_file_to_parse()
    parse_file(addr_filename, city_id=sys.argv[1], only_missing=only_missing)
    save_to_file(features)

else:
    print("""Usage:
    Command:
        all - generate address files for all cities
        download - download 'adresy.zip'
        remove - remove all data
        <municipality code> - generate address files for selected municipality
        (no command) - show menu to select city
    Switch:
        -m - save only addresses which are not in OSM
    """)

print(f"COMPLETE runtime {round(time.time() - runtime_start, 1)} seconds")

# minvskaddresstogeojson

Generate `.geojson` files with address points from [adresy.zip](https://proxy.freemap.sk/minvskaddress/adresy.zip)


## Installation

```
git clone https://codeberg.org/LiJu09/minvskaddresstogeojson
cd minvskaddresstogeojson
pip install -r requirements.txt
```


## Usage

`generate.py <switch> <command>`

### Command
- `all` - generate address files for all cities
- `download` - download `adresy.zip`
- `remove` - remove all data
- `<municipality code>` - generate address files for selected municipality
- `(no command)` - show menu to select city

### Switch
- `-m` - save only addresses which are not in OSM